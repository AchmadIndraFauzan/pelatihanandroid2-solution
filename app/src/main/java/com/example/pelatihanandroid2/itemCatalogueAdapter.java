package com.example.pelatihanandroid2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class itemCatalogueAdapter extends RecyclerView.Adapter<itemCatalogueAdapter.itemCatalogueViewHolder>{

    private ArrayList<ItemCatalogueData> dataList;
    Context context;

    public itemCatalogueAdapter (ArrayList<ItemCatalogueData> dataList, Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public itemCatalogueAdapter.itemCatalogueViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_cardview_catalogue, viewGroup, false);
        return new itemCatalogueViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull itemCatalogueAdapter.itemCatalogueViewHolder itemCatalogueViewHolder, int i) {

        itemCatalogueViewHolder.itemTitle.setText(dataList.get(i).getItemTitle());
        itemCatalogueViewHolder.itemDescription.setText(dataList.get(i).getItemDescription());
        itemCatalogueViewHolder.itemPoster.setImageResource(dataList.get(i).getItemPoster());
        itemCatalogueViewHolder.ratingBar.setRating(Float.parseFloat(dataList.get(i).getItemRating()));
        itemCatalogueViewHolder.itemGenre = dataList.get(i).getItemGenre();
        itemCatalogueViewHolder.imageSource = dataList.get(i).getItemPoster();
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class itemCatalogueViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDescription;
        ImageView itemPoster;
        RatingBar ratingBar;
        String itemGenre;
        int imageSource;

        public itemCatalogueViewHolder(@NonNull final View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.itemTitle);
            itemDescription = itemView.findViewById(R.id.itemDescription);
            itemPoster = itemView.findViewById(R.id.itemPoster);
            ratingBar = itemView.findViewById(R.id.itemRating);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(itemView.getContext(),
                            itemTitle.getText() + " Selected", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


}
