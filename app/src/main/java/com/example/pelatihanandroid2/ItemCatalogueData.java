package com.example.pelatihanandroid2;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemCatalogueData {

    private String itemTitle;
    private String itemDescription;
    private String itemRating;
    private String itemGenre;
    private int itemPoster;


    public ItemCatalogueData(String itemTitle, String itemDescription, String itemRating, String itemGenre, int itemPoster) {
        this.itemTitle = itemTitle;
        this.itemDescription = itemDescription;
        this.itemRating = itemRating;
        this.itemGenre = itemGenre;
        this.itemPoster = itemPoster;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public String getItemRating() {
        return itemRating;
    }

    public int getItemPoster() {
        return itemPoster;
    }

    public String getItemGenre() {
        return itemGenre;
    }
}
