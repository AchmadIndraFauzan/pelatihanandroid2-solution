package com.example.pelatihanandroid2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.TypedArray;
import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rv_movies;
    private String[] itemName;
    private String[] itemDescription;
    private String[] itemGenre;
    private String[] itemRating;
    private TypedArray itemPoster;
    private ArrayList<ItemCatalogueData> itemCatalogueDataArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv_movies = findViewById(R.id.rv_movies);

        prepareMovieItem();
        addMovieItem();
    }

    private void prepareMovieItem() {
        itemName = getResources().getStringArray(R.array.data_movie_name);
        itemDescription = getResources().getStringArray(R.array.data_description_movie);
        itemGenre = getResources().getStringArray(R.array.data_genre);
        itemRating = getResources().getStringArray(R.array.data_rating_movie);
        itemPoster = getResources().obtainTypedArray(R.array.data_poster_movie);
    }

    private void addMovieItem() {
        for (int i = 0; i < itemName.length; i++) {

            itemCatalogueDataArrayList.add(new ItemCatalogueData(
                    itemName[i],
                    itemDescription[i],
                    itemRating[i],
                    itemGenre[i],
                    itemPoster.getResourceId(i, 0)
            ));
        }

        rv_movies.setLayoutManager(new LinearLayoutManager(this));
        rv_movies.setAdapter(new itemCatalogueAdapter(itemCatalogueDataArrayList, this));

    }
}
